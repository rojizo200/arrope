
//Funcion que dado un producto, el sentido y el indice actual, cambia para adelante o para atras la foto
function cambiarImagen(idProducto, sentido){
    let indiceActual;
    let fotos=document.querySelectorAll("img[data-id='"+idProducto+"']");
    //EL INDICE ESTA EN EL ELEMENTO QUE NO TIENE EL style="display: block"
    for (let i = 0; i < fotos.length; i++) {
        const element = fotos[i];
        let display=element.getAttribute('style');
        if(display == "display: block"){
            indiceActual=i;
        }
    }

    indiceActual+=sentido;
    if(indiceActual==-1){indiceActual=fotos.length-1}
    if(indiceActual==fotos.length){indiceActual=0}
        

    for (let i = 0; i < fotos.length; i++) {
        const element = fotos[i];
        element.setAttribute('style','display: none');
    }

    fotos[indiceActual].setAttribute('style','display: block');

}