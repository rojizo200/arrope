$(document).ready(function () {

    $('#nombreProducto').keyup(function (e) {
        $('#nombreFinal').html(this.value);
    });

    $('#precioProducto').keyup(function (e) {
        $('#precioFinal').html(this.value);
    });

    $('#categoriaProducto').on('change', function () {
        $('#categoriaFinal').html(this.value);
    });

    $('#subCategoriaProducto').on('click', function () {
        console.log(this.value);
        $('#subCategoriaFinal').html(this.value);

    });

    $('#fotoProducto').on('change', function (e) {
        //ACA PUEDO ELIMINAR EL PLACEHOLDER ANTES DE PONER LA NUEVA IMAGEN

        // Leemos el archivo subido y se lo pasamos a nuestro fileReader
        

        let preview = document.getElementById('fotoFinal');
        preview.innerHTML='';
        for (i = 0; i < e.target.files.length; i++) {
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[i]);
            reader.onload = function () {
                preview,image = document.createElement('img');
                image.setAttribute('style', 'margin-left: 10px;margin-top: 10px; height: 200px');

                image.src = reader.result;


                //preview.innerHTML += '';
                preview.append(image);

            };
        }


        // Le decimos que cuando este listo ejecute el código interno
    });

   

    $('#cantidadProducto').keyup(function (e) {
        $('#cantidadFinal').html(this.value);
    });


    $(document).ready(function () {
         //ESTO HABILITA PARA QUE SE CARGUE LA PRIMERA OPCION
        $("#categoriaProducto").click(function () {
            $("#categoriaProducto").change();
        });

        $('#categoriaProducto').change(function () { 
            
            let categoria=$('#categoriaProducto').val();
            let url = $('#categoriaProducto').attr('data-url');
            console.log('La categoria es: ',categoria)
            $.ajax({
                type: "get",
                url: url,
                data: {
                    'categoria' : categoria
                },
                dataType: "json",
                success: function (response) {
                    objetivo=$('#subCategoriaProducto')
                    objetivo.html('');
                    for (let index = 0; index < response.length; index++) {
                        const subCategoria = response[index]['nombre'];
                        objetivo.append('<option value="'+subCategoria+'">'+subCategoria+'</option>')

                    }
                },
                error: function (error) { 
                    console.log(error)
                }
            });
        });
    });

    
})

//function cargarSubCategoria(select) { 
//    alert("Estoy aca adentro")
//        let categoria=$('#categoriaProducto').val();
//        let url = ""
//        $.ajax({
//            type: "get",
//            url: url,
//            data: {
//                'categoria' : categoria
//            },
//            dataType: "json",
//            success: function (response) {
//                console.log(response)
//            },
//            error: function (error) { 
//                console.log(error)
//            }
//        });
//}


