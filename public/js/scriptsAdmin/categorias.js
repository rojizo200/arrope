$(document).ready(function () {


    $('#modalSubCategoria').on('show.bs.modal', function (event) {
        let link = $(event.relatedTarget) // link que inicio el modal
        let modal = $(this);
        let accion = link.attr('data-accion');
        let nombre = link.attr('data-subcategoria');
        let categoria = link.attr('data-categoria');
        console.log("La categoria en el modal es: " + categoria)

        if (accion == "modificar") {
            modal.find('#tituloModalSubCategoria').html('Editar Sub-Categoria')
            modal.find('#nombreSubCategoria').val(nombre);
            modal.find('#auxiliarSubCategoria').val(nombre);
            $('#formSubCategoriaModificar').removeAttr('hidden');
            $('#formSubCategoriaEliminar').attr('hidden', 'true');

            modal.find('#footerModalSubCategoria').html('');
            modal.find('#footerModalSubCategoria').html(
                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>' +
                '<button type="button" class="btn btn-primary" id="modificarSubCategoriaButton"' +
                '    data-dismiss="modal" value=' + categoria + ' onclick="modificarSubCategoria(this)">Aceptar</button>'
            )
        } else {
            if (accion == "eliminar") {
                modal.find('#tituloModalSubCategoria').html('Eliminar Sub-Categoria')
                modal.find('#nombreSubCategoriaEliminar').html('Se eliminara la Sub-Categoria y productos de: <b>' + nombre + '</b>');
                modal.find('#formSubCategoriaEliminar').removeAttr('hidden');
                $('#formSubCategoriaModificar').attr('hidden', 'true');

                modal.find('#footerModalSubCategoria').html('');
                modal.find('#footerModalSubCategoria').html(
                    '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>' +
                    '<button type="button" class="btn btn-danger" data-nombre="' + nombre + '" value="' + categoria + '" onclick="eliminarSubCategoria(this)" id="eliminarCategoriaButton"' +
                    '    data-dismiss="modal">Eliminar</button>'
                );
            }
        }
    });


    //cuando se muestra el modal
    $('#modalCategoria').on('show.bs.modal', function (event) {
        let link = $(event.relatedTarget) // link que inicio el modal
        let modal = $(this);
        let accion = link.attr('data-accion');
        let nombre = link.attr('data-categoria');

        if (accion == "modificar") {
            modal.find('#tituloModalCategoria').html('Editar Categoria')
            modal.find('#nombreCategoria').val(nombre);
            modal.find('#auxiliarCategoria').val(nombre);
            //ahora tengo que insertarle el footer correspondiente
            $('#formularioModificar').removeAttr('hidden');
            $('#formularioEliminar').attr('hidden', 'true');

            modal.find('#footerModalCategoria').html('');
            modal.find('#footerModalCategoria').html(
                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>' +
                '<button type="button" class="btn btn-primary" onclick="modificarCategoria()" id="modificarCategoriaButton"' +
                '    data-dismiss="modal">Aceptar</button>'
            )
        } else {
            if (accion = "eliminar") {
                modal.find('#tituloModalCategoria').html('Eliminar Categoria')
                modal.find('#nombreCategoriaEliminar').html('Se eliminara la categoria, subCategorias y productos de: <b>' + nombre + '</b>');
                modal.find('#formularioEliminar').removeAttr('hidden');
                $('#formularioModificar').attr('hidden', 'true');

                modal.find('#footerModalCategoria').html('');
                modal.find('#footerModalCategoria').html(
                    '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>' +
                    '<button type="button" class="btn btn-danger" value="' + nombre + '" onclick="eliminarCategoria(this)" id="eliminarCategoriaButton"' +
                    '    data-dismiss="modal">Eliminar</button>'
                );
            }
        }
    });


    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });


    $('#modalNuevaCategoria').on('show.bs.modal', function (event) {
        let boton = $(event.relatedTarget) // link que inicio el modal
        let modal = $(this);

        if (boton.attr('id') == 'nuevaCategoriaButton') {
            modal.find('#tituloNuevaCategoria').html('Nueva Categoria')
            modal.find('#formNuevaCategoria').removeAttr('hidden');
            modal.find('#formNuevaSubCategoria').attr('hidden', 'true');
            modal.find('#footerModalNuevaCategoria').html('');
            modal.find('#footerModalNuevaCategoria').html(
                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>' +
                '<button type="button" class="btn btn-primary" onclick="cargarCategoria()" id="cargarCategoriaButton"' +
                '    data-dismiss="modal">Crear</button>'
            );

        } else {
            let categoria = $(boton).val()
            if (boton.attr('id') == 'nuevaSubCategoriaButton') {
                modal.find('#tituloNuevaCategoria').html('Nueva Sub-Categoria (' + categoria + ')')
                modal.find('#formNuevaSubCategoria').removeAttr('hidden');
                modal.find('#formNuevaCategoria').attr('hidden', 'true');
                modal.find('#footerModalNuevaCategoria').html('');
                modal.find('#footerModalNuevaCategoria').html(
                    '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>' +
                    '<button type="button" class="btn btn-primary" value="' + categoria + '" onclick="cargarSubCategoria(this)"id="cargarSubCategoriaButton"' +
                    '    data-dismiss="modal">Crear</button>'
                );
            }
        }
    })


});

function modificarCategoria() {
    let nombreViejo = $('#auxiliarCategoria').val();

    let nombreNuevo = $('#nombreCategoria').val();


    let datos = {
        'viejo': nombreViejo,
        'nuevo': nombreNuevo
    }
    var url = $('#formularioModificar').attr('action');
    $.ajax({
        type: "post",
        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        },
        url: url,
        data: datos,
        dataType: 'json',
        success: function (data) {
            listarCategorias(data)
        },

        error: function (error) {
            console.log(error);
        }
    });

}


function eliminarCategoria(boton) {
    let nombre = boton.value;
    let url = $('#formularioEliminar').attr('action');

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        data: { 'categoria': nombre },
        dataType: "json",
        success: function (data) {
            listarCategorias(data);
        },
        error: function (error) {
            console.log(error)
        }
    });
}

function listarCategorias(data) {
    let div = document.getElementById('divCategorias');
    $('#divCategorias').html('');
    for (index in data) {
        $('#divCategorias').append(
            '<div class="lista">' +
            '    <h2 class="heading mb-4">' +
            '        <a href="#" data-categoria="' + index + '" data-accion="modificar"' +
            '            data-toggle="modal" data-target="#modalCategoria">' + index + '</a>' +
            '' +
            '        <a href="#" data-categoria="' + index + '" data-toggle="modal"' +
            '            data-target="#modalCategoria" data-accion="eliminar">' +
            '            <span style="color: Tomato; font-size: 20px; margin-inline: 10px">' +
            '                <i class="fas fa-trash-alt"></i>' +
            '            </span>' +
            '        </a>' +
            '    </h2>' +
            '' +
            '    <ul id="' + index + '">'
        );
        for (var key in data[index]) {
            //console.log(data[index][key]['nombre'])
            $('#' + index).append(
                '<li>'+
                '    <a href="#" data-categoria="'+index+'" data-subcategoria="'+data[index][key]['nombre']+'" data-accion="modificar"'+
                '        data-toggle="modal" data-target="#modalSubCategoria">'+data[index][key]['nombre']+'</a>'+
                '    <a href="#" data-categoria="'+index+'" data-subcategoria='+data[index][key]['nombre']+' data-toggle="modal"'+
                '        data-target="#modalSubCategoria" data-accion="eliminar">'+
                '        <span style="color: Tomato; font-size: 15px; margin-inline: 10px">'+
                '            <i class="fas fa-trash-alt"></i>'+
                '        </span>'+
                '    </a>'+
                '</li>'
            );
        }
        $('#' + index).append('<button class="btn btn-primary mt-5" data-toggle="modal" data-target="#modalNuevaCategoria"value="'+index+'" id="nuevaSubCategoriaButton">Nueva sub-categoria</button>');
    }
}


function modificarSubCategoria(boton) {
    let nombreViejo = $('#auxiliarSubCategoria').val();
    let nombreNuevo = $('#nombreSubCategoria').val();
    let categoria = boton.value


    let datos = {
        'viejo': nombreViejo,
        'nuevo': nombreNuevo,
        'categoria': categoria
    }
    var url = $('#formSubCategoriaModificar').attr('action');
    $.ajax({
        type: "post",
        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        },
        url: url,
        data: datos,
        dataType: 'json',
        success: function (data) {
            listarCategorias(data)
        },

        error: function (error) {
            console.log(error);
        }
    });

}

function eliminarSubCategoria(boton) {
    let nombre = boton.getAttribute('data-nombre');
    let url = $('#formSubCategoriaEliminar').attr('action');
    let categoria = boton.value;

    datos = {
        'nombre': nombre,
        'categoria': categoria
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        data: datos,
        dataType: "json",
        success: function (data) {
            listarCategorias(data);
        },
        error: function (error) {
            console.log(error)
        }
    });
}


function cargarCategoria() {
    let modal = $('#modalNuevaCategoria');
    
    let nombreCategoria = modal.find('#nombreNuevaCategoria').val();
    let url = modal.find('#formNuevaCategoria').attr('action');
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        data: {
            "nombreCategoria": nombreCategoria,
        },
        dataType: "JSON",
        success: function (response) {
            listarCategorias(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
    
}

function cargarSubCategoria(boton) {
    let categoria = boton.value
    let modal = $('#modalNuevaCategoria')
    let nombreSubCategoria = modal.find('#nombreNuevaSubCategoria').val();
    let url = modal.find('#formNuevaSubCategoria').attr('action')
    $.ajax({
        type: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        data: {
            'categoria': categoria,
            'nombreSubCategoria': nombreSubCategoria
        },
        dataType: "json",
        success: function (response) {
            listarCategorias(response);
        },
        error: function (error) {
            console.log(error)
        }
    });
}


