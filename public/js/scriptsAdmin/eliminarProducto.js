//tengo que hacer un modal con una confirmacion si quiere eliminar el producto o no
//Si dice que si tengo que enviar el formulario.

$(document).ready(function () {
    
    $('#modalEliminarProducto').on('show.bs.modal', function (event) {
        modal=this
        let link = $(event.relatedTarget) // link que inicio el modal

        nombreProducto=link.attr('data-nombre');
        idProducto=link.attr('data-id');
        $('#nombreProductoEliminar').html('');
        $('#nombreProductoEliminar').html('Se eliminara el producto: <b>'+nombreProducto+'</b>');

        $(modal).find('#eliminarProductoButton').val(idProducto);
        
    });


    $('#eliminarProductoButton').click(function (e) { 
        idProducto=$(this).val();
        form=$('#formEliminarProducto');
        $(form).append('<input type="text" name="idProducto" id="idProducto" value="'+idProducto+'" hidden=true>')
        $(form).submit();
        
    });

});