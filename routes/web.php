<?php

use App\Http\Middleware\Authenticate;
use App\Http\Middleware\CheckAdmin;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('/tienda', 'ClienteController@getTienda')->name('tienda');

Route::get('/tienda/producto/{producto}', 'ProductoController@detalleProducto')->name('detalleProducto');

Route::get('/contacto', function () {
    return view('cliente.contact');
})->name('contacto');

Route::get('/acerca', function () {
    return view('cliente.about');
})->name('acerca');

Route::get('/blog', function () {
    return view('cliente.blog');
})->name('blog');

Route::get('/carrito', function () {
    return view('cliente.cart');
})->name('carrito');

Route::get('/checkout', function () {
    return view('cliente.checkout');
})->name('checkout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//------------ AJAX ------------------

Route::get('/tienda/{categoria}','ClienteController@getTiendaCategoria')->name('getXCategoria');
Route::get('/tienda/{categoria}/{subCategoria}','ClienteController@getTiendaCategoriaSubCategoria')->name('getXCatYSub');

//-------------- ADMIN ---------------

Route::middleware([Authenticate::class])->group(function(){
    Route::get('/admin','AdminController@adminIndexGet')->name('adminIndex');

});


Route::middleware([CheckAdmin::class,Authenticate::class])->group(function(){
    //PROBAR SI ESTAS QUE TIENEN EL MISMO NOMBRE NO DA PROBLEMA

    Route::get('admin/nuevoUsuario', 'AdminController@nuevoUsuarioGet')->name('adminNewUser');
    Route::post('admin/nuevoUsuario', 'AdminController@nuevoUsuarioPost');
    Route::get('admin/editarUsuario', 'AdminController@editarUsuarioGet')->name('adminEditUser');
    Route::post('admin/editarUsuario', 'AdminController@editarUsuarioPost');
    Route::post('admin/eliminarUsuario', 'AdminController@eliminarUsuario')->name('adminDeleteUserPost');

    Route::get('admin/nuevoProducto', 'AdminController@nuevoProductoGet')->name('adminNewProductGet');
    Route::post('admin/nuevoProducto', 'ProductoController@nuevoProductoPost')->name('adminNewProductPost');
    Route::get('admin/modificarProducto/{id}', 'AdminController@modificarProductoGet')->name('adminModificarProductoGet');
    Route::post('admin/modificarProducto', 'ProductoController@modificarProductoPost')->name('adminModificarProductoPost');
    

    
    //PARA EDITAR O ELIMINAR UN PRODUCTO, LE MUESTRO LA TIENDA NORMAL PERO CON LOS BOTONES PROPIOS
    Route::get('admin/tienda', 'AdminController@tiendaAdmin')->name('adminTienda'); 
    Route::get('admin/tienda/{categoria}','AdminController@getTiendaCategoria')->name('adminGetXCategoria');
    Route::get('admin/tienda/{categoria}/{subCategoria}','AdminController@getTiendaCategoriaSubCategoria')->name('adminGetXCatYSub');
    Route::post('admin/eliminarProducto', 'ProductoController@eliminarProducto')->name('adminDeleteProduct');

    Route::get('admin/clientes', 'AdminController@listarClientes')->name('adminGetClientes');
    Route::get('admin/usuarios', 'AdminController@listarUsuarios')->name('adminGetUsuarios');

    //ESTA NO TENDRIA QUE SER USADA SI O SI POR EL ADMINISTRADOR
    Route::get('admin/categorias', 'AdminController@listarCategorias')->name('adminGetCategorias');


    Route::post('admin/ajax/modificarCategoria','AjaxController@modificarCategoriaPost')->name('modificarCategoriaAjax');
    Route::post('admin/ajax/eliminarCategoria','AjaxController@eliminarCategoriaPost')->name('eliminarCategoriaAjax');

    Route::post('admin/ajax/modificarSubCategoria','AjaxController@modificarSubCategoriaPost')->name('modificarSubCategoriaAjax');
    Route::post('admin/ajax/eliminarSubCategoria','AjaxController@eliminarSubCategoriaAjax')->name('eliminarSubCategoriaAjax');
    Route::post('admin/ajax/nuevaCategoria','AjaxController@nuevaCategoriaAjax')->name('nuevaCategoriaAjax');
    Route::post('admin/ajax/nuevaSubCategoria','AjaxController@nuevaSubCategoriaAjax')->name('nuevaSubCategoriaAjax');
    Route::get('admin/ajax/getSubCategorias','AjaxController@getSubCategorias')->name('getSubCategoriasAjax');


});