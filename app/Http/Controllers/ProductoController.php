<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class ProductoController extends Controller
{
 
    public function nuevoProductoPost(Request $request){
        //PROCESAR TODA LA INFORMACION PARA SUBIR EL PRODUCTO, HACER UNA TRANSACCION
        $nombresFotos=array();

        $producto=new Producto();
        $producto->nombre=$request->input('nombre');
        $producto->precio=$request->input('precio');
        $producto->categoria=$request->input('categoria');
        $producto->subCategoria=$request->input('subCategoria');
        $producto->descripcion=$request->input('descripcion');
        if($request->has('cantidad')){
            $producto->stock=$request->input('cantidad');
        }
        
        if($producto->save()){
            //Si se guardo correctamente, necesito el id
            $id=DB::table('productos')->select('id')->orderByDesc('id')->first();   //devuelve un objeto
            foreach ($request->file('foto') as $foto) {
                DB::table('fotosxproductos')->insert(
                    ['idProducto'=>$id->id,'nombre'=>$foto->hashName()]
                );
                $foto->store('uploads','public');
            }
            //retornar mensaje de exito
        }else{
            //retornar algun error
        }

        return redirect(route('adminNewProductGet'));
    }

    //ESTA FUNCION LISTA LOS PRODUCTOS QUE ESTAN EN LA BASE DE DATOS Y SE LOS MANDA A LA TIENDA
    public function listarAllProductos(){
        $rows=DB::table('productos')->orderBy('id','desc')->paginate(6);
        return $rows;
    }

    //devuelve todas las fotos de un producto
    public function getFotosProducto($id){
        return DB::table('fotosxproductos')->select('nombre')->where('idProducto', '=' ,$id)->get();
    }

    public function getProducto($id){
        $row=DB::table('productos')->select()->where('id', '=' ,$id)->get()->first();
        return $row;
    }


    /*
        1- Eliminar todas las fotos de la tabla que tenga el id del producto
        2- Actualizar todos los dato
        3- Si todo eso funciono eliminar las fotos posta
        4- Sino hacer un rollback
    */
    public function modificarProductoPost(Request $request){
        if($request->input('id')!=null){
            DB::beginTransaction();
                try {
                    DB::table('productos')
                        ->where('id', $request->input('id'))
                        ->update(
                            [
                                'nombre' => $request->input('nombre'),
                                'precio' => $request->input('precio'),
                                'categoria' => $request->input('categoria'),
                                'subCategoria' => $request->input('subCategoria'),
                                'stock' => $request->input('cantidad'),
                                'descripcion'=>$request->input('descripcion')
                            ]
                        );

                    if($request->input('cambiador')=='true'){   //Significa que cambio las fotos
                        //tengo que eliminar las fotos viejas de la base de datos
                        $fotos=DB::table('fotosxproductos')->select('nombre')->where('idProducto','=',$request->input('id'))->get();
                        
                        DB::table('fotosxproductos')->where('idProducto','=',$request->input('id'))->delete();
                        //ahora subo las fotos nuevas
                        foreach ($request->file('foto') as $foto) {
                            DB::table('fotosxproductos')->insert(
                                ['idProducto'=>$request->input('id'),'nombre'=>$foto->hashName()]
                            );
                            $foto->store('uploads','public');
                        }
                        //ahora elimino los archivos fisicos
                        foreach ($fotos as $foto) {
                            Storage::disk('public')->delete('uploads/'.$foto->nombre);
                        }
                    }
                    DB::commit();
                    return redirect(route('adminTienda'));
                } catch (\Throwable $th) {
                    DB::rollBack();
                    echo($th);
                }
        }else{
            return back();
        }
    }
    //tengo que eliminar el producto, eliminar las fotos tanto de la base de datos como los archivos
    public function eliminarProducto(Request $request){
        $id=$request->input('idProducto');
        if($id != null){
            DB::beginTransaction();
            try {
                $nombresFotos=DB::table('fotosXProductos')->select('nombre')->where('idProducto','=',$id)->get();
                DB::table('productos')->where('id','=',$id)->delete();
    
                foreach ($nombresFotos as $foto) {
                    Storage::disk('public')->delete('uploads/'.$foto->nombre);
                }
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                echo($th);      
            }
        }
        return back();
    }

    public function getXCategoria($categoria){
        $productos=DB::table('productos')
            ->where('categoria','=',$categoria)
            ->orderBy('id','desc')
            ->paginate(6);
        return $productos;
    }

    public function getXCatYSub($categoria,$subCategoria){
        $productos=DB::table('productos')
            ->where('categoria','=',$categoria)
            ->where('subCategoria','=',$subCategoria)
            ->orderBy('id','desc')
            ->paginate(6);
        return $productos;
    }

    public function detalleProducto(Request $request,$idProducto){
        
        $producto=DB::table('productos')->where('id','=',$idProducto)->first();
        if($producto != null){
            $fotos=$this->getFotosProducto($idProducto);
    
            $categoria=DB::table('productos')->select('categoria')->where('id','=',$idProducto)->first();
            $productos=$this->getXCategoria($categoria->categoria);
            foreach ($productos as $var) {
                $fotosRelacionados[$var->id]=$this->getFotosProducto($var->id);
            }
    
    
            return view('cliente.product-single',compact('producto','fotos','productos','fotosRelacionados'));
        }else{
            return back();
        }
    }
}
