<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    //
    public function getTienda(Request $r){    //aca mando a la tienda pero con las opciones de administrador
        $controlador=new ProductoController();
        $productos=$controlador->listarAllProductos();
        
        //tengo que obtener la foto del producto para pasarsela tambien
        $fotos=array();
        foreach ($productos as $producto) {
            $fotos[$producto->id]=$controlador->getFotosProducto($producto->id);
        }
        $controladorCategorias=new CategoriasController();
        $categorias= $controladorCategorias->getAllCategorias();

        return view('cliente.shop',compact('productos','categorias','fotos'));
    }

    public function getTiendaCategoria(Request $r,$categoria){
        $controlador=new ProductoController();
        $productos=$controlador->getXCategoria($categoria);
        $fotos=array();
        foreach ($productos as $producto) {
            $fotos[$producto->id]=$controlador->getFotosProducto($producto->id);
        }
        $controladorCategorias=new CategoriasController();
        $categorias= $controladorCategorias->getAllCategorias();

        return view('cliente.shop',compact('productos','categorias','fotos','categoria'));
    }

    public function getTiendaCategoriaSubCategoria(Request $r,$categoria,$subCategoria){
        $controlador=new ProductoController();
        $productos=$controlador->getXCatYSub($categoria,$subCategoria);
        $fotos=array();
        foreach ($productos as $producto) {
            $fotos[$producto->id]=$controlador->getFotosProducto($producto->id);
        }
        $controladorCategorias=new CategoriasController();
        $categorias= $controladorCategorias->getAllCategorias();

        return view('cliente.shop',compact('productos','categorias','fotos','categoria','subCategoria'));
    }
}
