<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function adminIndexGet(Request $r){
        if(Auth::user()->administrador==1){
            return redirect(route('adminTienda'));
        }else{
            return redirect(route('login'));        //Aca como esta logueado lo manda a home, para entrar como admin, tenes que cerrar sesion antes
        }
    }

    public function tiendaAdmin(Request $r){    //aca mando a la tienda pero con las opciones de administrador
        $administrador=true;
        $controlador=new ProductoController();
        $productos=$controlador->listarAllProductos();
        
        $fotos=array();
        foreach ($productos as $producto) {
            $fotos[$producto->id]=$controlador->getFotosProducto($producto->id);
        }
        $controladorCategorias=new CategoriasController();
        $categorias=$controladorCategorias->getAllCategorias();
        return view('cliente.shop',compact('administrador','productos','categorias','fotos'));
    }

    public function getTiendaCategoria(Request $r,$categoria){
        $administrador=true;
        $controlador=new ProductoController();
        $productos=$controlador->getXCategoria($categoria);
        $fotos=array();
        foreach ($productos as $producto) {
            $fotos[$producto->id]=$controlador->getFotosProducto($producto->id);
        }
        $controladorCategorias=new CategoriasController();
        $categorias= $controladorCategorias->getAllCategorias();

        return view('cliente.shop',compact('productos','categorias','fotos','administrador'));
    }

    public function getTiendaCategoriaSubCategoria(Request $r,$categoria,$subCategoria){
        $administrador=true;
        $controlador=new ProductoController();
        $productos=$controlador->getXCatYSub($categoria,$subCategoria);
        $fotos=array();
        foreach ($productos as $producto) {
            $fotos[$producto->id]=$controlador->getFotosProducto($producto->id);
        }
        $controladorCategorias=new CategoriasController();
        $categorias= $controladorCategorias->getAllCategorias();

        return view('cliente.shop',compact('productos','categorias','fotos','administrador'));
    }

    public function nuevoProductoGet (Request $r){
        $controladorCategorias=new CategoriasController();
        $categorias=$controladorCategorias->getCategorias();
        return view('administrador.newProduct',compact('categorias'));
    }

    public function modificarProductoGet(Request $r,$id=null){
        if($id!=null){
            $controlador= new ProductoController();
            $producto=$controlador->getProducto($id);
            $fotos=$controlador->getFotosProducto($id);
            $controladorCategorias=new CategoriasController();
            $categorias=$controladorCategorias->getCategorias();
            if($producto != null && $fotos->isNotEmpty()){  //ES DECIR ENCONTRO EL PRODUCTO y sus fotos
                return view('administrador.modifyProduct',compact('producto','fotos','categorias'));
            }else{
                echo("No encontro nada");
                //TODO: HACER MANEJO DE ERRORES
            }
        }
    }

    public function listarCategorias(Request $r){
        $controlador=new CategoriasController();
        $categorias=$controlador->getAllCategorias();
        return view('administrador.categorias',compact('categorias'));
    }

    

}
