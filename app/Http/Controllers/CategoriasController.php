<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriasController extends Controller
{
    //
    public function updateCategoria($viejo,$nuevo){
        $error=false;
        DB::beginTransaction();
        try {
            DB::table('categorias')
                ->where('nombre','=',$viejo)
                ->update(
                    [
                        'nombre' => $nuevo
                    ]
                    );
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            $error=true;
            echo($th);
        }

        return $error;
    }
    //Esta funcion devuelve las cateorias y subCategorias

    //1- Obtener los nombres de las categorias, iterar obteniendo las subcategorias, crear un map gigante

    //2- Obtener todos los datos de las subCategorias agrupados por categoria --------------->ESTA
    public function getAllCategorias(){
        $resultado=array();
        //ESTO TRAE UN ARRAY DE OBJETOS CATEGORIAS
        $categorias=DB::table('categorias')->select('nombre')->orderBy('nombre','asc')->get();
        foreach ($categorias as $categoria) {
            $resultado[$categoria->nombre]=DB::table('subcategorias')->where('categoria','=',$categoria->nombre)->select('nombre','categoria')->get()->toArray();
        }
        return $resultado;
    }
    /* 
        Solo trae las categorias de la base de datos
    */
    public function getCategorias(){
        $categorias=DB::table('categorias')->select('nombre')->orderBy('nombre','asc')->get();
        return $categorias;
    }

    public function getSubCategorias($categoria){
        $resultado=DB::table('subcategorias')->select('nombre')->where('categoria','=',$categoria)->get();
        return $resultado;
    }


    public function eliminarCategoria($categoria){
        $error=false;
        DB::beginTransaction();
        try {
            DB::table('categorias')->where('nombre','=',$categoria)->delete();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            echo($th);
            $error=true;
        }
        return $error;
    }

    public function updateSubCategoria($viejo,$nuevo,$categoria){
        $error=false;
        DB::beginTransaction();
        try {
            DB::table('subcategorias')
                ->where('nombre','=',$viejo)
                ->where('categoria','=',$categoria)
                ->update(
                    [
                        'nombre' => $nuevo
                    ]
                    );
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            $error=true;
            echo($th);
        }

        return $error;
    }

    public function eliminarSubCategoria($nombre,$categoria){
        $error=false;
        DB::beginTransaction();
        try {
            DB::table('subcategorias')
                ->where('nombre','=',$nombre)
                ->where('categoria','=',$categoria)
                ->delete();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            echo($th);
            $error=true;
        }
        return $error;
    }
    public function nuevaCategoria($nombre){
        $error=false;
        try {
            DB::table('categorias')
                ->insert(['nombre' => $nombre]);
        } catch (\Throwable $th) {
            $error=true;
            echo($th);
        }
    }

    public function nuevaSubCategoria($categoria,$nombre){
        $error=false;
        try {
            DB::table('subcategorias')
                ->insert(['nombre' => $nombre,'categoria'=>$categoria]);
        } catch (\Throwable $th) {
            $error=true;
            echo($th);
        }
    }
}
