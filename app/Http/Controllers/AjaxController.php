<?php

namespace App\Http\Controllers;

use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AjaxController extends Controller
{
    public function modificarCategoriaPost(Request $request){
        
        $controlador=new CategoriasController();

        $error=$controlador->updateCategoria($request->input('viejo'),$request->input('nuevo'));
        if($error){
            return response()->json('error'['Huboo un error']);
        }

        $categorias=$controlador->getAllCategorias();
        return response()->json($categorias);
    }

    public function eliminarCategoriaPost(Request $request){
        $controlador=new CategoriasController();
        $error=$controlador->eliminarCategoria($request->input('categoria'));

        $categorias=$controlador->getAllCategorias();
        return response()->json($categorias);
    }

    public function modificarSubCategoriaPost(Request $request){
        $controlador=new CategoriasController();
        $viejo=$request->input('viejo');
        $nuevo=$request->input('nuevo');
        $categoria=$request->input('categoria');
        $error=$controlador->updateSubCategoria($viejo,$nuevo,$categoria);
        
        if($error){
            return response()->json('error'['Huboo un error']);
        }

        $categorias=$controlador->getAllCategorias();
        return response()->json($categorias);
    
    }

    public function eliminarSubCategoriaAjax(Request $request){
        $controlador=new CategoriasController();
        $nombre=$request->input('nombre');
        $categoria=$request->input('categoria');
        $error=$controlador->eliminarSubCategoria($nombre,$categoria);
        if($error){
            return response()->json('error'['Huboo un error']);
        }

        $categorias=$controlador->getAllCategorias();
        return response()->json($categorias);
    }

    public function nuevaCategoriaAjax(Request $request){
        $controlador=new CategoriasController();
        $nombreCategoria=$request->input('nombreCategoria');
        $error=$controlador->nuevaCategoria($nombreCategoria);

        if($error){
            return response()->json('error'['Huboo un error']);
        }

        $categorias=$controlador->getAllCategorias();
        return response()->json($categorias);
    }

    public function nuevaSubCategoriaAjax(Request $request){
        $controlador=new CategoriasController();
        $categoria=$request->input('categoria');
        $nombreSubCategoria=$request->input('nombreSubCategoria');
        $error=$controlador->nuevaSubCategoria($categoria,$nombreSubCategoria);
    
        if($error){
            return response()->json('error'['Huboo un error']);
        }

        $categorias=$controlador->getAllCategorias();
        return response()->json($categorias);
    }

    public function getSubCategorias(Request $request){
        $controlador=new CategoriasController();
        $subCategorias=$controlador->getSubCategorias($request->input('categoria'));
        
        return response()->json($subCategorias);
    }

    public function getProdXCat(Request $request,$categoria){
        $controlador=new ProductoController();
        $productos=$controlador->getXCategoria($categoria);

        return response()->json($productos);
    }

    public function getProdXCatYSubCat(Request $request,$categoria,$subCategoria){
        $controlador=new ProductoController();
        $productos=$controlador->getXCatYSub($categoria,$subCategoria);

        return response()->json($productos);
    }

    

}
