<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

<link rel="stylesheet" href="{{ url('/css/open-iconic-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ url('/css/animate.css')}}">

<link rel="stylesheet" href="{{ url('/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{ url('/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{ url('/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{ url('/css/aos.css')}}">
<link rel="stylesheet" href="{{ url('/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{ url('/css/bootstrap-datepicker.css')}}">
<link rel="stylesheet" href="{{ url('/css/jquery.timepicker.css')}}">

<link rel="stylesheet" href="{{ url('/css/flaticon.css')}}">
<link rel="stylesheet" href="{{ url('/css/icomoon.css')}}">
<link rel="stylesheet" href="{{ url('/css/style.css')}}">
