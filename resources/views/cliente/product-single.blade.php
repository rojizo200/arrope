<!DOCTYPE html>
<html lang="en">

<head>
	<title>Arrope - Emprendimiento de tejido</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	@include('estilos')
	<style>
		/* Next & previous buttons */
		.prev,
		.next {
			cursor: pointer;
			position: absolute;
			top: 50%;
			width: auto;
			margin-top: -22px;
			padding: 16px;
			color: white !important;
			font-weight: bold;
			font-size: 18px;
			transition: 0.6s ease;
			border-radius: 0 3px 3px 0;
			user-select: none;
		}

		/* Position the "next button" to the right */
		.next {
			right: 0;
			border-radius: 3px 0 0 3px;
		}

		/* On hover, add a black background color with a little bit see-through */
		/**.prev:hover,
		.next:hover {
			background-color: rgba(0, 0, 0, 0.8);
		}**/
	</style>
</head>

<body class="goto-here">
	@include('navBar')
	<!-- END nav -->

	<div class="hero-wrap hero-bread" style="background-image: url('/images/bg_6.jpg');">
		<div class="container">
			<div class="row no-gutters slider-text align-items-center justify-content-center">
				<div class="col-md-9 ftco-animate text-center">
					<h1 class="mb-0 bread">Detalle del Producto</h1>
				</div>
			</div>
		</div>
	</div>

	<section class="ftco-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 mb-5 ftco-animate">
					
					@for($i = 0; $i < count($fotos); $i++)
					@if ($i==0)
					<img class="img-fluid"
					src="{{ url('storage/uploads/'.$fotos[$i]->nombre)}}"
					data-id="{{$producto->id}}" alt="Foto producto" style="display: block">
					@else
					<img class="img-fluid"
					src="{{ url('storage/uploads/'.$fotos[$i]->nombre)}}"
					data-id="{{$producto->id}}" alt="Foto producto" style="display: none">
					@endif
					@endfor
					<div class="overlay">
						<a class="prev" onclick="cambiarImagen({{$producto->id}},-1)">&#10094;</a>
						<a class="next" style="right: 12px"onclick="cambiarImagen({{$producto->id}},1)">&#10095;</a>
					</div>
				</div>
				<div class="col-lg-6 product-details pl-md-5 ftco-animate">
					<h3>{{$producto->nombre}}</h3>
					
					@if(isset($producto->precio))
					<p class="price"><span>Precio: ${{$producto->precio}}</span></p>
					@endif
					<p>{{$producto->descripcion}}</p>

					<div style="margin-block: 10%" class="float-right">
						<p><a href="cart.html" class="btn btn-black py-3 px-5">Agregar al carrito</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section bg-light">
		<div class="container">
			<div class="row justify-content-center mb-3 pb-3">
				<div class="col-md-12 heading-section text-center ftco-animate">
					<h2 class="mb-4">Productos relacionados</h2>
					<p>Estos son los productos relacionados segun tu busqueda</p>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				@foreach($productos as $var)
				@if($var->id != $producto->id)
				<div class="col-sm-6 col-md-6 col-lg-4 ftco-animate">
					<div class="product">
						<a href="{{route('detalleProducto',$var->id)}}" class="img-prod">
							@for($i = 0; $i < count($fotosRelacionados[$var->id]); $i++)
								@if($i==0)
								
									<img class="img-fluid" data-id="{{$var->id}}"
									src="{{ url('storage/uploads/'.$fotosRelacionados[$var->id][$i]->nombre)}}" alt="Foto producto"
									style="display: block">
									
								@else
									<img class="img-fluid"
									src="{{ url('storage/uploads/'.$fotosRelacionados[$var->id][$i]->nombre)}}"
									data-id="{{$var->id}}" alt="Foto producto" style="display: none">
								@endif
							@endfor
								<div class="overlay">
									<a class="prev" onclick="cambiarImagen({{$var->id}},-1)">&#10094;</a>
									<a class="next" onclick="cambiarImagen({{$var->id}},1)">&#10095;</a>
								</div>
						</a>

						<div class="text py-3 px-3">
							<h3><a href="#">{{$var->nombre}}</a></h3>
							<div class="d-flex">
								<div class="pricing">
									<p class="price"><span class="price-sale">${{$var->precio}}</span></p>
								</div>
							</div>
							<p class="bottom-area d-flex px-3">
								<a href="#" class="add-to-cart text-center py-2 mr-1">
									<span>Al carrito</span>
								</a>
								<a href="#" class="buy-now text-center py-2">
									<span>Comprar ahora</span>
								</a>
							</p>
						</div>
					</div>
				</div>
				@endif
				@endforeach
			</div>
		</div>
	</section>
	{{-- 
	
	<footer class="ftco-footer bg-light ftco-section">
		<div class="container">
			<div class="row">
				<div class="mouse">
					<a href="#" class="mouse-icon">
						<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
					</a>
				</div>
			</div>
			<div class="row mb-5">
				<div class="col-md">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2">Winkel</h2>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
						<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
							<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
							<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
							<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md">
					<div class="ftco-footer-widget mb-4 ml-md-5">
						<h2 class="ftco-heading-2">Menu</h2>
						<ul class="list-unstyled">
							<li><a href="#" class="py-2 d-block">Shop</a></li>
							<li><a href="#" class="py-2 d-block">About</a></li>
							<li><a href="#" class="py-2 d-block">Journal</a></li>
							<li><a href="#" class="py-2 d-block">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2">Help</h2>
						<div class="d-flex">
							<ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
								<li><a href="#" class="py-2 d-block">Shipping Information</a></li>
								<li><a href="#" class="py-2 d-block">Returns &amp; Exchange</a></li>
								<li><a href="#" class="py-2 d-block">Terms &amp; Conditions</a></li>
								<li><a href="#" class="py-2 d-block">Privacy Policy</a></li>
							</ul>
							<ul class="list-unstyled">
								<li><a href="#" class="py-2 d-block">FAQs</a></li>
								<li><a href="#" class="py-2 d-block">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2">Have a Questions?</h2>
						<div class="block-23 mb-3">
							<ul>
								<li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain
										View, San Francisco, California, USA</span></li>
								<li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929
											210</span></a></li>
								<li><a href="#"><span class="icon icon-envelope"></span><span
											class="text">info@yourdomain.com</span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			--}}
			<div class="row">
				<div class="col-md-12 text-center">

					<p>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<script>
							document.write(new Date().getFullYear());
						</script> All rights reserved | This template is made with <i class="icon-heart color-danger"
							aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					</p>
				</div>
			</div>
		</div>
	</footer>


	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
			<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
			<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
				stroke="#F96D00" /></svg></div>


	@include('scripts')
	<script src="{{asset('/js/scriptsCliente/sliderManager.js')}}"></script>



	<script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>

</body>

</html>