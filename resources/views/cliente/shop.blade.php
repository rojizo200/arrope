<!DOCTYPE html>
<html lang="en">

<head>
	<title>Winkel - Free Bootstrap 4 Template by Colorlib</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}" />

	@include('estilos')
	<style>
		/* Next & previous buttons */
		.prev,
		.next {
			cursor: pointer;
			position: absolute;
			top: 50%;
			width: auto;
			margin-top: -22px;
			padding: 16px;
			color: white !important;
			font-weight: bold;
			font-size: 18px;
			transition: 0.6s ease;
			border-radius: 0 3px 3px 0;
			user-select: none;
		}

		/* Position the "next button" to the right */
		.next {
			right: 0;
			border-radius: 3px 0 0 3px;
		}

		/* On hover, add a black background color with a little bit see-through */
		/**.prev:hover,
		.next:hover {
			background-color: rgba(0, 0, 0, 0.8);
		}**/
	</style>
</head>

<body class="goto-here">
	@if(isset($administrador))
	@if($administrador==true)
	@include('administrador.adminNavBar')
	@endif
	@else
	@include('navBar')
	@endif
	<!-- END nav -->

	<div class="hero-wrap hero-bread" style="background-image: url('/images/bg_6.jpg');">
		<div class="container">
			<div class="row no-gutters slider-text align-items-center justify-content-center">
				<div class="col-md-9 ftco-animate text-center">
					<h1 class="mb-0 bread">Colleccion de productos</h1>
				</div>
			</div>
		</div>
	</div>

	<section class="ftco-section bg-light">
		<div class="container">
			@if(isset($categoria))
				@if(isset($subCategoria))
					<p><b>{{$categoria}} / {{$subCategoria}}</b></p>
				@else
					<p><b>{{$categoria}} / </b></p>
				@endif
			@else
				<p><b>Todos los productos</b></p>
				
			@endif
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-10 order-md-last">
					<div class="row" id="divProductos">
						<!-- A partir de aca se repite -->
						@foreach($productos as $producto)
						<div class="col-sm-6 col-md-6 col-lg-4 ftco-animate">
							<div class="product">
								<a href="{{route('detalleProducto',$producto->id)}}" class="img-prod">
									<img class="img-fluid" data-id="{{$producto->id}}"

										src="{{ url('storage/uploads/'.$fotos[$producto->id][0]->nombre)}}"
										alt="Foto producto" style="display: block">
									@for($i = 1; $i < count($fotos[$producto->id]); $i++) <img class="img-fluid"
										src="{{ url('storage/uploads/'.$fotos[$producto->id][$i]->nombre)}}"
										data-id="{{$producto->id}}" alt="Foto producto" style="display: none">
									@endfor
										<div class="overlay">
											<a class="prev" onclick="cambiarImagen({{$producto->id}},-1)">&#10094;</a>
											<a class="next" onclick="cambiarImagen({{$producto->id}},1)">&#10095;</a>
										</div>
								</a>

								<div class="text py-3 px-3">
									<h3><a href="#">{{$producto->nombre}}</a></h3>
									<div class="d-flex">
										<div class="pricing">
											<p class="price"><span class="price-sale">${{$producto->precio}}</span></p>
										</div>
									</div>
									<p class="bottom-area d-flex px-3">
										@if(isset($administrador))
										@if($administrador==true)
										<a href="{{route('adminModificarProductoGet',$producto->id)}}"
											class="add-to-cart text-center py-2 mr-1">
											<span>Modificar</span>
										</a>
										<a href="#" data-toggle="modal" data-id={{$producto->id}} data-nombre="{{$producto->nombre}}" data-target="#modalEliminarProducto" class="buy-now text-center py-2">
											<span>Eliminar</span>
										</a>
										@endif
										@else
										<a href="#" class="add-to-cart text-center py-2 mr-1">
											<span>Al carrito</span>
										</a>
										<a href="#" class="buy-now text-center py-2">
											<span>Comprar ahora</span>
										</a>
										@endif
									</p>
								</div>
							</div>
						</div>
						@endforeach

						<!-- Hasta aca -->

					</div>
					{{$productos->links()}}
				</div>

				<div class="col-md-4 col-lg-2 sidebar">
					<div class="sidebar-box-2">

						@if(isset($administrador) and $administrador==true)
							@foreach($categorias as $categoria => $subCategorias)
								<h2 class="heading mb-4"><a  href="{{route('adminGetXCategoria',$categoria)}}">{{$categoria}}</a></h2>
								<ul>
								@foreach($subCategorias as $subCategoria)
									<li><a href="{{ route( 'adminGetXCatYSub',[$categoria,$subCategoria->nombre] ) }}"> {{$subCategoria->nombre}} </a></li>
								@endforeach
								</ul>
							@endforeach
						@else	
							@foreach($categorias as $categoria => $subCategorias)
								<h2 class="heading mb-4"><a  href="{{route('getXCategoria',$categoria)}}">{{$categoria}}</a></h2>
								<ul>
								@foreach($subCategorias as $subCategoria)
								<li><a href="{{ route( 'getXCatYSub',[$categoria,$subCategoria->nombre] ) }}"> {{$subCategoria->nombre}} </a></li>
								@endforeach
								</ul>
							@endforeach
						@endif


					</div>
				</div>
			</div>
		</div>
	</section>

	<footer class="ftco-footer bg-light ftco-section">
		<div class="container">
			
			<div class="row">
				<div class="col-md-12 text-center">

					<p>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<script>
							document.write(new Date().getFullYear());
						</script> All rights reserved | This template is made with <i class="icon-heart color-danger"
							aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					</p>
				</div>
			</div>
		</div>
	</footer>



	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
			<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
			<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
				stroke="#F96D00" /></svg></div>


	@include('scripts')
	<script src="{{asset('/js/scriptsCliente/sliderManager.js')}}"></script>
	@if(isset($administrador))
	@if($administrador==true)
	<script src="{{asset('/js/scriptsAdmin/eliminarProducto.js')}}"></script>
	@endif
	@endif

</body>

	@if(isset($administrador))
	@if($administrador==true)
	<div class="modal fade" id="modalEliminarProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Eliminar producto</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="cuerpoModalSubCategoria">
					<form action={{ route('adminDeleteProduct') }} method="post"id="formEliminarProducto">
						@csrf
						<label id="nombreProductoEliminar"></label>
					</form>
				</div>
				<div class="modal-footer" id="footerModalSubCategoria">
					<button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button class="btn btn-danger" id="eliminarProductoButton">Eliminar</button>
				</div>
			</div>
		</div>
	</div>
	@endif
	@endif

</html>