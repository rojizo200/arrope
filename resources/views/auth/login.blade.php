@extends('layouts.app')


@section('css')
    <link rel="stylesheet" href="{{ url('/css/estilosAutenticacion/style.css')}}">
@endsection

@section('content')
<div>
    <!-- TENGO QUE HACER ESTE PADING MAS CHICO-->
    <section class="login_box_area p_100">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="login_box_img">
						<img class="img-fluid" src="images/product-1.jpg" alt="No se pudo encontrar">
						<div class="hover">
							<h4>Nuevo en nuestro sitio?</h4>
							<p>Tenemos nuevos productos todos los dias</p>
                        <a class="main_btn" href="{{route('register')}}">Registrarse</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="login_form_inner">
                        <h3>Inicia sesion para continuar</h3>
                        
                        <form class="row login_form" action="{{ route('login') }}" method="post" id="contactForm">
                            @csrf
							
                            <div class="col-md-12 form-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('Email') }}" required autocomplete="Email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            
                            <div class="col-md-12 form-group">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-12 form-group">
                                <div class="creat_account">
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                            
                            <div class="col-md-12 form-group">
                                <button type="submit" class="btn submit_btn">
                                    {{ __('Inciar Sesion') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('¿Olvidaste la contraseña?') }}
                                    </a>
                                @endif
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
