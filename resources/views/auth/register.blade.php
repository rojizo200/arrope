@extends('layouts.app')


@section('css')
    <link rel="stylesheet" href="{{ url('/css/estilosAutenticacion/style.css')}}">
@endsection

@section('content')
<div>
    <!-- TENGO QUE HACER ESTE PADING MAS CHICO-->
    <section class="login_box_area p_100">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="login_box_img">
						<img class="img-fluid" src="images/product-1.jpg" alt="No se pudo encontrar">
						<div class="hover">
							<h4>¿Ya tienes una cuenta?</h4>
                        <a class="main_btn" href="{{route('login')}}">Iniciar Sesion</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="login_form_inner">
                        <h3>Registrate</h3>
                        
                        <form class="row login_form" action="{{ route('register') }}" method="post" id="contactForm">
                            @csrf


                            <div class="col-md-12 form-group">    
                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" placeholder="Nombre" value="{{ old('nombre') }}" required autocomplete="nombre" >

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-12 form-group">   
                                <input id="apellido" type="text" class="form-control @error('apellido') is-invalid @enderror" name="apellido" placeholder="Apellido"value="{{ old('apellido') }}" required autocomplete="nombre" >

                                @error('apellido')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-12 form-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-12 form-group">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
							
                            
                            <div class="col-md-12 form-group">    
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña" required autocomplete="new-password">
                            </div>
    
                            <div class="col-md-12 form-group">
                                <button type="submit" class="btn submit_btn">
                                    {{ __('Registrarse') }}
                                </button>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
