<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
    <a class="navbar-brand" href="{{route('index')}}">Arrope</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active"><a href="{{route('index')}}" class="nav-link">Home</a></li>
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tienda</a>
          <div class="dropdown-menu" aria-labelledby="dropdown04">
              <a class="dropdown-item" href="{{route('tienda')}}">Tienda</a>
            <a class="dropdown-item" href="{{route('carrito')}}">Carrito</a>
          <a class="dropdown-item" href="{{route('checkout')}}">Checkout</a>
          </div>
        </li>
      <li class="nav-item"><a href="{{route('acerca')}}" class="nav-link">Acerca</a></li>
          <li class="nav-item"><a href="{{route('blog')}}" class="nav-link">Blog</a></li>
          <li class="nav-item"><a href="{{route('contacto')}}" class="nav-link">Contacto</a></li>
          <li class="nav-item cta cta-colored"><a href="{{route('carrito')}}" class="nav-link"><span class="icon-shopping_cart"></span>[0]</a></li>

        </ul>
      </div>
    </div>
  </nav>