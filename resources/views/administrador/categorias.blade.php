<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Categorias</title>
    @include('estilos')


    <style>
        .lista {
            margin-inline: 50px;
            margin-block: 30px;
        }

        .lista a {
            color: black;
        }
    </style>

</head>

<body>
    @include('administrador.adminNavBar')

    <div class="container">
        <div>
            <h3>
                Categorias
            </h3>
        </div>

        <div class="container">
            <div class="row" id='divCategorias'>

                
                
                
                
                @foreach($categorias as $categoria => $subCategorias)
                <div class="lista col-lg-4 col-sm-12">
                    <h2 class="heading mb-4">
                        <a href="#" data-categoria="{{$categoria}}" data-accion="modificar"
                            data-toggle="modal" data-target="#modalCategoria">{{$categoria}}</a>

                        <a href="#" data-categoria="{{$categoria}}" data-toggle="modal"
                            data-target="#modalCategoria" data-accion="eliminar">
                            <span style="color: Tomato; font-size: 20px; margin-inline: 10px">
                                <i class="fas fa-trash-alt"></i>
                            </span>
                        </a>
                    </h2>

                    <ul>
                        @foreach($subCategorias as $subcategoria)
                        <li>
                            <a href="#" data-categoria="{{$categoria}}" data-subcategoria="{{$subcategoria->nombre}}" data-accion="modificar"
                                data-toggle="modal" data-target="#modalSubCategoria">{{$subcategoria->nombre}}</a>
                            <a href="#" data-categoria="{{$categoria}}" data-subcategoria={{$subcategoria->nombre}} data-toggle="modal"
                                data-target="#modalSubCategoria" data-accion="eliminar">
                                <span style="color: Tomato; font-size: 15px; margin-inline: 10px">
                                    <i class="fas fa-trash-alt"></i>
                                </span>
                            </a>
                        </li>

                        @endforeach
                        <button class="btn btn-primary mt-5" data-toggle="modal" data-target="#modalNuevaCategoria"value="{{$categoria}}" id="nuevaSubCategoriaButton">Nueva sub-categoria</button>
                    </ul>
                </div>
                @endforeach
            </div>

            <div class="float-right mb-3">
                <button class="btn btn-primary" id="nuevaCategoriaButton"data-toggle="modal" data-target="#modalNuevaCategoria">Nueva categoria</button>
            </div>

        </div>

    </div>
    @include('scripts')

    <script src="{{asset('/js/scriptsAdmin/categorias.js')}}"></script>
</body>

<div class="modal fade" id="modalCategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tituloModalCategoria"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="cuerpoModalCategoria">
                <form action="{{route('modificarCategoriaAjax')}}" id="formularioModificar" hidden=true>
                    <label for="nombreCategoria">Nombre: </label>
                    <input type="text" value="" id="nombreCategoria" name="nombreCategoria" class="form-control">
                    <input type="text" id="auxiliarCategoria" hidden>
                </form>
                <form action="{{route('eliminarCategoriaAjax')}}" id="formularioEliminar" hidden=true>
                    <label id="nombreCategoriaEliminar"></label>
                </form>
            </div>
            <div class="modal-footer" id="footerModalCategoria">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalSubCategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tituloModalSubCategoria"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="cuerpoModalSubCategoria">
                <form action="{{route('modificarSubCategoriaAjax')}}" id="formSubCategoriaModificar" hidden=true>
                    <label for="nombreSubCategoria">Nombre: </label>
                    <input type="text" value="" id="nombreSubCategoria" name="nombreSubCategoria" class="form-control">
                    <input type="text" id="auxiliarSubCategoria" hidden>
                </form>
                <form action="{{route('eliminarSubCategoriaAjax')}}" id="formSubCategoriaEliminar" hidden=true>
                    <label id="nombreSubCategoriaEliminar"></label>
                </form>
            </div>
            <div class="modal-footer" id="footerModalSubCategoria">

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalNuevaCategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tituloNuevaCategoria"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="cuerpoNuevaCategoria">
                <form action="{{route('nuevaCategoriaAjax')}}" id="formNuevaCategoria" hidden=true>
                    <label for="nombreNuevaCategoria">Nombre: </label>
                    <input type="text" id="nombreNuevaCategoria" name="nombreCategoria" class="form-control">
                </form>
                <form action="{{route('nuevaSubCategoriaAjax')}}" id="formNuevaSubCategoria" hidden=true>
                    <label for="nombreNuevaSubCategoria">Nombre: </label>
                    <input type="text" value="" id="nombreNuevaSubCategoria" name="nombreSubCategoria" class="form-control">
                </form>
            </div>
            <div class="modal-footer" id="footerModalNuevaCategoria">

            </div>
        </div>
    </div>
</div> 

</html>