{{-- 
    LOS PRODUCTOS TIENEN:

    NOMBRE
    PRECIO
    CATEGORIA       --TRAER DE LA BASE DE DATOS
    SUB-CATEGORIA   --TRAER DE LA BASE DE DATOS
    CANTIDAD(OPCIONAL)
    FOTO(OPCIONAL)
--}}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Arrope-Admin</title>
    @include('estilos')
    <style>
        .submit_btn {
            float: right;
            text-transform: uppercase;
            display: block;
            border-radius: 0px;
            width: 100%;
            text-transform: uppercase;
            margin-top: 20px;
            margin-bottom: 20px;
            cursor: pointer;
            display: inline-block;
            background: #1641ff;
            padding: 0px 30px;
            color: #fff;
            font-family: "Roboto", sans-serif;
            font-size: 14px;
            font-weight: 500;
            line-height: 38px;
            border: 1px solid #1641ff;
            border-radius: 0px;
            outline: none !important;
            box-shadow: none !important;
            text-align: center;
            border: 1px solid #1641ff;
            cursor: pointer;
            transition: all 300ms linear 0s;
        }

        .submit_btn:hover {
            background: transparent;
            color: #1641ff;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
</head>

<body>
    @include('administrador.adminNavBar')


    <div class="container">
        <div>
            <h3>
                Nuevo Producto
            </h3>
        </div>
        <div class="container col-lg-6 col-sm-12 float-left">
            <form id="formNewProduct" action="" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="nombreProducto">Nombre del producto</label>
                    <input type="text" class="form-control" name="nombre" id="nombreProducto" placeholder="Nombre: "
                        required>
                </div>
                <div class="form-group ">
                    <label for="fotoProducto">Foto del producto</label>
                    <input type="file" class="form-control-file" name="foto[]" id="fotoProducto"
                        placeholder="Foto del producto" required multiple="multiple">
                </div>
                <div class="form-group ">
                    <label for="precioProducto">Precio del producto</label>
                    <input type="number" class="form-control" name="precio" id="precioProducto" placeholder="$ Precio"
                        required min="0" step="0.01">
                </div>
                <div class="form-group">
                    <label for="descripcionProducto">Descripcion</label>
                    <textarea name="descripcion" id="descripcionProducto" rows="6" class="form-control" required>

                    </textarea>
                </div>
                <div class="form-group ">
                    <label for="categoriaProducto">Categoria</label>
                    <select class="form-control" name="categoria" data-url="{{route('getSubCategoriasAjax')}}" id="categoriaProducto">
                        @foreach($categorias as $categoria)
                        <option value="{{$categoria->nombre}}">{{$categoria->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group ">
                    <label for="subCategoriaProducto">Sub-categoria</label>
                    <select class="form-control" name="subCategoria" id="subCategoriaProducto">
                        
                    </select>
                </div>
                <div class="form-group ">
                    <label for="cantidadProducto">Stock inicial (Opcional)</label>
                    <input type="number" class="form-control" name="cantidad" id="cantidadProducto"
                        placeholder="Cantidad" min="0" step="1">
                </div>

            </form>
        </div>
        <div class="container col-lg-6 col-sm-12 float-right" style="margin-bottom: 20px">
            <div class="form-group">
                <label for="nombreFinal">Nombre: </label>
                <h4 id="nombreFinal"></h4>
            </div>
            <div class="form-group" id="fotoFinal">


            </div>

            <div class="form-group col-lg-6 col-sm-12 float-left">
                <label for="precioFinal">Precio: </label>
                <h3 id="precioFinal"></h3>
            </div>
            <div class="form-group col-lg-6 col-sm-12 float-right">
                <label for="categoriaFinal">Categoria: </label>
                <h3 id="categoriaFinal"></h3>
            </div>
            <div class="form-group col-lg-6 col-sm-12 float-left">
                <label for="subCategoriaFinal">Sub-Categoria: </label>
                <h3 id="subCategoriaFinal"></h3>
            </div>
            <div class="form-group col-lg-6 col-sm-12 float-right">
                <label for="cantidadFinal">Stock: </label>
                <h3 id="cantidadFinal"></h3>
            </div>
        </div>
        <div class="form-group ">
            <button type="submit" class="btn submit_btn" onclick="event.preventDefault();
            document.getElementById('formNewProduct').submit();">
                Cargar Producto
            </button>
        </div>
    </div>
    @include('scripts')
    <script src="{{ asset('/js/scriptsAdmin/productoDinamico.js')}}"></script>

</body>

</html>