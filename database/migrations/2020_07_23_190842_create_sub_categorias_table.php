<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subCategorias', function (Blueprint $table) {
            $table->string('nombre');
            $table->string('categoria');
            $table->foreign('categoria')->references('nombre')->on('categorias')->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['nombre','categoria']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_categorias');
    }
}
