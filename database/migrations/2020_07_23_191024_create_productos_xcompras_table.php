<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosXcomprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //VOY A GUARDAR EL NOMBRE Y CATEGORIA DEL PRODUCTO POR LAS DUDAS QUE DESPUES SE BORRE, EL ID NO SIRVE
        Schema::create('productosXcompra', function (Blueprint $table) {
            $table->unsignedBigInteger('idProducto');
            $table->unsignedBigInteger('idCompra');
            $table->string('producto');
            $table->string('categoria');
            $table->foreign('idProducto')->references('id')->on('productos')->onDelete('cascade');
            $table->foreign('idCompra')->references('id')->on('compras')->onDelete('cascade');
            $table->foreign('producto')->references('nombre')->on('productos'); //TESTEO QUE PASA SI NO SE PONE NADA
            $table->foreign('categoria')->references('nombre')->on('categorias');
            $table->primary(['idProducto','idCompra']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos_xcompras');
    }
}
